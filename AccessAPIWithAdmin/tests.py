import datetime
from django.urls import reverse
from django.test import TestCase
from django.utils import timezone

from .models import Cycle


def create_cycles(cycle_text, days):
    time = timezone.now() + datetime.timedelta(days=days)
    return Cycle.objects.create(cycle_text=cycle_text, cycle_date=time)


class CycleViewTests(TestCase):
    def test_index_view_with_no_cycles(self):
        """
        If no cycles exist, an appropriate message should be displayed.
        """
        response = self.client.get(reverse('AccessAPIWithAdmin:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No cycles are available.")
        self.assertQuerysetEqual(response.context['latest_cycle_list'], [])