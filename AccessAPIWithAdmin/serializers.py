from rest_framework import serializers

from .models import Details


class DetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Details
        fields = [
            'details_number',
            'details_serial_number',
            'details_serial_src',
            'details_lot_number',
            'details_cycle_number',
            'details_control_frequency',
            'details_status',
            'details_type',
            'details_well_number',
            'details_estimated_time',
            'details_text',
        ]
