from django.urls import path, include

from . import views
from AccessAPIWithAdmin.views import RESTDetailsList, RESTDetailsDetail

app_name = 'AccessAPIWithAdmin'
urlpatterns = [
    path('', views.index, name='index'),
    path('devices/<int:device_id>/', views.cycles_device, name='devices'),
    path('details/<int:details_id>/', views.cycles_details, name='results'),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    path("rest-api/details/", RESTDetailsList.as_view(), name="rest_details_list"),
    path(
        "rest-api/details/<uuid:pk>/", RESTDetailsDetail.as_view(), name="rest_details_detail"
    ),
]
