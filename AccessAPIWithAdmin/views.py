from django.http import HttpResponse, HttpResponseRedirect
from django.utils.safestring import SafeString
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

from AccessAPIWithAdmin.models import Cycle, Details, Device

from rest_framework import generics
from .serializers import DetailsSerializer


# from models import Details


def index(request):
    latest_device_list = Device.objects.filter().order_by('-id')[:5]
    context = {
        'latest_device_list': latest_device_list,
        'css_name': 'main.css',
        'logo_name': 'aspcloud.png',
        'image_name': 'asplogo.png',
    }
    return render(request, 'index.html', context)


def cycles_device(request, device_id):
    cycle_device = get_object_or_404(Cycle, pk=device_id)
    latest_cycle_device_list = Cycle.objects.filter(cycle_device_id=device_id).order_by('-cycle_date')[:5]
    context = {
        'latest_device_list': latest_cycle_device_list,
        'cycle': cycle_device,
        'css_name': 'main.css',
        'logo_name': 'aspcloud.png',
        'image_name': 'asplogo.png',
    }
    return render(request, 'devices.html', context)


def cycles_details(request, details_id):
    cycle_details = get_object_or_404(Details, pk=details_id)
    latest_cycle_details_list = Details.objects.filter(details_number_id=details_id)
    context = {
        'latest_details_list': latest_cycle_details_list,
        'cycle': cycle_details,
        'css_name': 'main.css',
        'logo_name': 'aspcloud.png',
        'image_name': 'asplogo.png',
    }
    return render(request, 'details.html', context)


class RESTDetailsList(generics.ListCreateAPIView):
    queryset = Details.objects.all()
    serializer_class = DetailsSerializer

    def get_view_name(self):
        return "Details List"


class RESTDetailsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Details.objects.all()
    serializer_class = DetailsSerializer

    def get_view_name(self):
        return "Details Detail"
