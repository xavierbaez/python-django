from django.db import models
from django.utils.translation import gettext_lazy as _


class Device(models.Model):
    device_number = models.IntegerField
    device_text = models.CharField(max_length=200)

    def __str__(self):
        return self.device_text


class Cycle(models.Model):
    cycle_number = models.IntegerField
    cycle_text = models.CharField(max_length=200)
    cycle_date = models.DateTimeField('date published')
    cycle_device = models.ForeignKey(Device, on_delete=models.CASCADE)
    cycle_serial_number = models.IntegerField

    def __str__(self):
        return self.cycle_text


class Details(models.Model):
    details_number = models.ForeignKey(Cycle, on_delete=models.CASCADE)
    details_serial_number = models.CharField(max_length=200)
    details_serial_src = models.CharField(max_length=200)
    details_lot_number = models.CharField(max_length=200)
    details_cycle_number = models.CharField(max_length=200)
    details_control_frequency = models.CharField(max_length=200)
    details_status = models.CharField(max_length=200)
    details_type = models.CharField(max_length=200)
    details_well_number = models.CharField(max_length=200)
    details_estimated_time = models.DateTimeField('estimated_time')
    details_text = models.CharField(max_length=200)

    def __str__(self):
        return self.details_text


class Meta:
    verbose_name = _('Detail')
    verbose_plural = _('Details')
    unique_together = ['cycle_performer', 'cycle_title']

    def __init__(self):
        self.details_serial_number = None
        self.details_number = None

    def __str__(self):
        return f'{self.details_number} - {self.details_serial_number}'